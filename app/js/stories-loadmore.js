$(document).ready(function() {
  "use strict";
  
  var winDow = $(window);
  
  $('.stories_loadmore').click(function(){
    
    
    // Needed variables
    var $container = $('.isotope-storyhome');

    $container.isotope({
      itemSelector: '.storycard-mb-5',
    });
		var button = $(this),
		    data = {
          'action': 'stories_loadmore',
          'query': stories_loadmore_params.posts, // that's how we get params from wp_localize_script() function          
          'page' : stories_loadmore_params.current_page,
          'post_type' : stories_loadmore_params.post_type
        };
        // console.log('Storie : ' +data);
        console.log(data);
		$.ajax({
			url : stories_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
        
				button.text('Loading...'); // change the button text, you can also add a preloader image
			},
			success : function( data ){
        

				if( data ) { 

          var $items = $(data);
          

					// button.text( 'More posts' ).prev().before(data); // insert new posts
					button.text( 'More posts' ).prev().before( $container.append($items).isotope('appended',$items) ); // insert new posts
					stories_loadmore_params.current_page++;
 
					if ( stories_loadmore_params.current_page == stories_loadmore_params.max_page ) 
						button.remove(); // if last page, remove the button
 
					// you can also fire the "post-load" event here if you use a plugin that requires it
					// $( document.body ).trigger( 'post-load' );
				} else {
					button.remove(); // if no data, remove the button as well
        }
        
			}
		});
	});
});