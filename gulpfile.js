'use strict';
var gulp = require('gulp');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-csso');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var imagemin = require('gulp-imagemin');
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;
var uglify = require('gulp-uglify');
var pump = require('pump');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');

// Static Server + watching scss/html files
gulp.task('serve', ['sass', 'bootstrap', 'templates', 'images', 'compress', 'scripts'], function() {

  // browserSync({server: './assets'});
  browserSync.init({
    // server: "",
    startPath:'./',
    server: {        
        baseDir: ["./assets"]
    },
    files: [
        {
            match: ["app/templates/*.pug"],
            fn: function (event, file) {
                /** Custom event handler **/

                console.log(event, file);

            }, options: {
              ignored: '*.txt'
            }
        }
    ]
  });

  gulp.watch("app/scss/*.scss", ['sass']);  
  gulp.watch("app/bootstrap/*.scss", ['bootstrap']);
  gulp.watch("app/js/*.js", ['compress-js']);
  gulp.watch("app/js/*.js", ['scripts-js']);
  gulp.watch("app/templates/*.pug", ['pug']);
  gulp.watch("apps/images/*", ['images-watch']);
});

var onError = function(err) {
  notify.onError({
    title:    "Gulp",
    subtitle: "Failure!",
    message:  "Error: <%= error.message %>",
    sound:    "Basso"
  })(err);
  this.emit('end');
};
var sassOptions = {
  outputStyle: 'expanded'
};
var prefixerOptions = {
  browsers: ['last 2 versions']
};

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
  return gulp.src('app/scss/*.scss')    
    .pipe(plumber({errorHandler: onError}))
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer(prefixerOptions))
    .pipe(minifyCSS())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest("assets/css"))
    .pipe(reload({stream: true}));
});

gulp.task('bootstrap', function () {
  return gulp.src('app/bootstrap/*.scss')
    .pipe(plumber())
    .pipe(sass({errLogToConsole: true}))
    .on('error', catchErr)
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(gulp.dest('assets/css'));
});

function catchErr(e) {
  console.log(e);
  this.emit('end');
}

gulp.task('pug', ['templates'], reload);
gulp.task('templates', function(){
  var YOUR_LOCALS = {
    "message": "This app is powered by gulp.pug recipe for BrowserSync"
  };

  return gulp.src([
      'app/templates/*.pug'
    ])
    .pipe(pug({
      locals: YOUR_LOCALS
    }))
    .pipe(gulp.dest('./assets'))
    .pipe(reload({stream: true}));
});

// Compile : Js minify
gulp.task('compress-js', ['compress'], reload);
gulp.task('compress', function (cb) {
  pump([
        gulp.src(['./app/js/main.js']),        
        // uglify(),
        gulp.dest('assets/js')
    ],
    cb
  );
});

gulp.task('scripts-js', ['scripts'], reload);
gulp.task('scripts', function() {
  gulp.src([
    'app/js/jquery.min.js', 
    'app/js/jquery.easing.1.3.js', 
    'app/js/tether.min.js',
    'app/js/bootstrap.min.js', 
    'app/js/imagesloaded.pkgd.js',
    'app/js/isotope.pkgd.min.js',
    'app/js/jquery.magnific-popup.js',
    'app/js/owl.carousel.min.js',    
    'app/js/jssocials.js',
    'app/js/jquery.tubular.1.0.js',    
    'app/js/jquery.mb.YTPlayer.js', 
    'app/js/collapzion.js',     
   ])
    .pipe(concat('plugin.js'))
    .pipe(uglify())
    .pipe(gulp.dest('assets/js'))
});

gulp.task('minify-css', ['minify'], reload);
gulp.task('minify', function() {
  gulp.src([
    'app/css/*.css'    
   ])
    .pipe(concat('plugin.css'))
    .pipe(uglify())
    .pipe(autoprefixer())  
    .pipe(gulp.dest('assets/css'))
});

gulp.task('css', ['styles'], reload);
gulp.task('styles', function () {
  return gulp.src([''])
    .pipe(autoprefixer())    
    .pipe(minifyCSS())    
    .pipe(gulp.dest('assets/css'))
    .pipe(reload({stream: true}));
});

// Compile : Images and reduce
gulp.task('images-watch', ['images'], reload);
gulp.task('images', function(){
	return gulp.src('app/images/*')
		.pipe(imagemin())
    .pipe(gulp.dest('assets/images'))
    .pipe(reload({stream: true}));
});
// Compile : Copy
gulp.task('copy', function(){
});

gulp.task('plugins', function(){
  return gulp
    .app(['app/plugins/*/*/*'])
    .pipe(gulp.dest('assets/plugins'));
});

gulp.task('default', ['serve']);