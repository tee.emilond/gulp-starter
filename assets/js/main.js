$(function(e) {
  "use strict";

	/*-------------------------------------------------*/
	/* =  Sticky Header
	/*-------------------------------------------------*/
	var window_view = $(window);
		
	/*-------------------------------------
	Top menu - fixed
	-------------------------------------*/
	window_view.on('scroll', function () {
		var winTop = window_view.scrollTop(),
			headerFixed = $(".header--transparent");

		if (winTop >= 150) {
			headerFixed.addClass("header--sticky");
		} else {
			headerFixed.removeClass("header--sticky");
		}

		/*-------------------------------------
		Hide Header on on scroll down
		-------------------------------------*/		
	})

  $(".search-menu").on("click", function(e) {
    $(".hsearch").toggleClass("active");
  });

  var homeOwl = $('.slider__home');
	homeOwl.owlCarousel({
		items: 1,
		loop: false,
		dots: false,
		margin: 0,
		autoplay: false,
		smartSpeed: 500,					
		lazyLoad: true,
		afterAction: function(current) {
			// current.find('video').get(0).play();
		}	
	});
  
  var whatWeOffOwl = $('.whatweoffer__slider');
	whatWeOffOwl.owlCarousel({
		items: 1,
		loop: false,
		dots: false,
		nav: true,
		navContainer: '.whatweoffer__nav',
		navText: ['<span class="icon-chevron-thin-left"></span>', '<span class="icon-chevron-thin-right"></span>'],
		margin: 0,
		autoplay: false,
		smartSpeed: 500,					
		lazyLoad: true,
		afterAction: function(current) {
			// current.find('video').get(0).play();
		}	
	});
  
  var CaseStudiesOwl = $('.casestudies__slider');
	CaseStudiesOwl.owlCarousel({
		items: 4,
		loop: false,
		dots: true,
		margin: 0,
		autoplay: false,
		smartSpeed: 500,					
		lazyLoad: true,
		responsive:{
			0:{
					items:2,
					nav:true
			},
			600:{
					items: 3,
					nav:false
			},
			1000:{
					items: 4
			}
		}
	});
	
	var tesimonailOwl = $('.tesimonail__slider');
	tesimonailOwl.owlCarousel({
		items: 1,
		loop: false,
		dots: true,
		margin: 0,
		autoplay: false,
		smartSpeed: 500,					
		lazyLoad: true,
		afterAction: function(current) {
			// current.find('video').get(0).play();
		}	
	});
 
 
});