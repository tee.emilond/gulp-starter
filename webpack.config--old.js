const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');


const pug = {
  test: /\.pug$/,
  use: ['html-loader?attrs=false', 'pug-html-loader']
};

const config = {
  entry: './src/home/home.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].bundle.js'
  },
  module: {
    rules: [pug]
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './src/home/home.pug',
      inject: false
    })
    ,
    new HtmlWebpackPlugin({
      filename: 'contact.html',
      template: './src/contact/contact.pug',
      inject: false
    })
 ]
};
module.exports = config;